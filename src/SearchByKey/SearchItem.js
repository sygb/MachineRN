import React, { Component } from 'react';
import { ListItem, Text, View, Icon } from 'native-base';
import { StyleSheet, TouchableOpacity} from "react-native";
import moment from 'moment';

export default class SearchItem extends Component {
    render() {
        let status = "新增"
        if (this.props.item.wxStatus == "WX"){
            status = "维修中"
        } else if (this.props.item.wxStatus == "FN"){
            status = "结案"
        } else if (this.props.item.wxStatus == "CN") {
            status = "撤单"
        }else{
            status = "新增"
        }
        return (
            <ListItem>
                <TouchableOpacity onPress={() => {
                    this.props.navigation.navigate('SearchInfo',
                        {
                            wxid: this.props.item.wxid
                        })
                }}>
                    <View style={{flexDirection:"row"}}>
                        <View style={style.kindStyle}>
                            <Text>{this.props.item.kind == "WX" ? "维修" : "保养"}</Text>
                        </View>
                        <View style={{ marginLeft: 10, paddingRight: 10 }}>
                            <Text>
                                {this.props.item.wxid}
                            </Text>
                            <Text>
                                {this.props.item.macName}{this.props.item.modelNo}
                            </Text>
                            <Text style={{ fontSize: 16, color: "#666" }}>
                                {this.props.item.mtncContent}
                            </Text>
                            <Text style={{ fontSize: 14, color: "#666" }}>
                                {this.props.item.wxStatus == "WX" ? this.props.item.recName + "," : ""}
                                {moment(this.props.item.mtncDate).format("MM-DD HH:mm:ss")}
                                {status}
                            </Text>
                            
                        </View>
                    </View>
                </TouchableOpacity>
            </ListItem>

        );
    }
}

const style = StyleSheet.create({
    kindStyle: {
        width: 50,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        borderColor: "lightblue",
        borderWidth: 1,
        borderRadius: 100,
        marginTop: 10,
        marginVertical: 10,
    }
})