import React, { Component } from 'react';
import { View, TouchableNativeFeedback, StyleSheet} from 'react-native';
import { Container, Header, Item, Input, Icon, Text, Body,List} from 'native-base';
import { DotIndicator } from 'react-native-indicators';

import SearchItem from './SearchItem';

export default class SearchBarExample extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isLast: false,
            list: [],
            sr: 1,
            rn: 10,
            keyword:""
        };
    }

    handelLoadList = () => {
        if (!global.isUrl) {
            alert('请先设置url');
            return
        }
        if (!global.isLogin) {
            alert('请先登录');
            return
        }
        this.setState({ loading: true });

        url = global.urltype + global.urlpath + ":" + global.urlport + "/api/MachineWX/getListByKey";

        para = 'key=' + this.state.keyword
            + '&pageIndex=' + this.state.sr
            + '&pageSize=' + this.state.rn;
        url += '?' + para;

        setTimeout(() => {
            return fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': global.token
                }
            }).then(response => response.json())
                .then(json => {
                    var newstate = {};
                    if (json.count > (this.state.sr - 1) * this.state.rn) {
                        newstate.list = this.state.list.concat(json.data);
                    }
                    if (json.count > this.state.sr * this.state.rn) {
                        newstate.sr = this.state.sr + 1;
                    } else {
                        newstate.isLast = true;
                    }
                    newstate.loading = false;
                    this.setState(newstate);
                })
                .catch(error => {
                    this.setState({ loading: false });
                    console.error(error);
                });
        }, 600)

    }
    handleSearch=()=>{
        if(this.state.keyword==""){
            alert("请输入关键词");
            return;
        }
        this.setState({
            loading: false,
            isLast: false,
            list: [],
            sr: 1,
        })
        setTimeout(() => {
            this.handelLoadList();
        }, 200);
    }
    render() {
        StatusBar = <View/>
        if (this.state.loading) {
            StatusBar = <DotIndicator color='lightgray' />
        } else {
            if (this.state.isLast) {
                StatusBar = <Text>没有更多资料了</Text>
            } else {
                if(this.state.keyword !=""){
                    StatusBar = <TouchableNativeFeedback onPress={this.handelLoadList}>
                        <Text style={{ margin: 30 }}>加载更多</Text>
                    </TouchableNativeFeedback>
                }
            }
        }
        return (
            <Container>
                <Header searchBar rounded style={{ backgroundColor: this.props.bgcolor}}>
                    <Item>
                        <Input placeholder="设备号或者问题描述" value={this.state.keyword} onChangeText={(keyword) => { this.setState({ keyword })}}/>
                        <Icon name="ios-search" onPress={this.handleSearch}/>
                    </Item>
                </Header>
                <Body>
                    <List>
                        {
                            this.state.list.map((itemObj, idx) => {
                                return (
                                    <SearchItem key={idx} item={itemObj}
                                        navigation={this.props.navigation}
                                    />
                                )
                            })
                        }
                    </List>
                    <View style={style.statusBarView}>
                        {StatusBar}
                    </View>
                </Body>
                
            </Container>
        );
    }
}

const style = StyleSheet.create({
    statusBarView: {
        justifyContent: 'center',
        alignItems: "center",
        height: 50,
    }
})