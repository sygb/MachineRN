import React, { Component } from 'react';
import { Alert, View, Image, StyleSheet, PixelRatio, TouchableOpacity} from 'react-native';
import { Container, Content, Card, CardItem, Text, Body, Button, Left, Right, List, ListItem } from 'native-base';
import moment from 'moment';
import ImageShow from '../components/comm/ImageShow';
export default class RepairInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            info: {},
            list: [],
            modalVisible: false,
        };
    }
    componentDidMount() {
        const { navigation } = this.props;
        //查询详情
        url = global.urltype + global.urlpath + ":" + global.urlport + "/api/MachineWX/getInfo";
        para = 'wxid=' + navigation.getParam('wxid');
        url += '?' + para;
        return fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': global.token
            }
        }).then(response => response.json())
            .then(json => {
                if (json.recode == 1) {
                    this.setState({ info: json.data });
                    this.loadList(navigation.getParam('wxid'));
                }
            })
            .catch(error => {
                console.error(error);
            });
    }
    loadList = (wxid) => {
        //查询维修记录
        url = global.urltype + global.urlpath + ":" + global.urlport + "/api/MachineWXDetail/getList";
        para = 'wxid=' + wxid;
        url += '?' + para;
        return fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': global.token
            }
        }).then(responselist => responselist.json())
            .then(jsonlist => {
                if (jsonlist.recode == 1) {
                    this.setState({
                        list: jsonlist.list
                    });
                }
            })
            .catch(error => {
                console.error(error);
            });
    }
    handleToggleModalVisible = () => {
        this.setState({
            modalVisible: !this.state.modalVisible
        })
    }
    render() {
        let imgArray = [];
        if (JSON.stringify(this.state.info) != '{}') {
            //图片
            imgArray = this.state.info.imgPath.split("^");
            info = (
                <Content>
                    <Card>
                        <CardItem header>
                            <Text>{this.state.info.wxid}</Text>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Text>
                                    设备编号:{this.state.info.macID}
                                </Text>
                                <Text>
                                    设备名称:{this.state.info.macName}
                                </Text>
                                <Text>
                                    设备型号:{this.state.info.modelNo}
                                </Text>
                                <Text>
                                    问题描述
                                </Text>
                                <Text>
                                    {this.state.info.mtncContent}
                                </Text>
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>{moment(this.state.info.mtncDate).format("YYYY-MM-DD HH:mm:ss")}</Text>
                        </CardItem>
                        <CardItem style={infoStyle.ImgContainer}>
                            {
                                imgArray.map((itemObj, idx) => {
                                    return (
                                        <View style={[infoStyle.Img, infoStyle.ImgContainer, { margin: 10 }]}>
                                            <TouchableOpacity onPress={this.handleToggleModalVisible}>
                                                <Image style={infoStyle.Img} key={idx}
                                                    source={{ uri: global.urltype + global.urlpath + ":" + global.urlport + itemObj }}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    )
                                })
                            }
                        </CardItem>
                    </Card>
                    <List style={{ borderTopColor: "#ddd", borderTopWidth: 1 }}>
                        {this.state.list.map((itemObj, idx) => {
                            return (
                                <ListItem key={idx} style={itemObj.kind == "WX" ? infoStyle.wx : infoStyle.new}>
                                    <Body>
                                        <Text>{moment(itemObj.createTime).format("YYYY-MM-DD HH:mm:ss")}</Text>
                                        <Text>描述:{itemObj.wxContent}</Text>
                                    </Body>
                                </ListItem>
                            )
                        })}
                    </List>
                    <ImageShow
                        modalVisible={this.state.modalVisible}
                        toggleModalVisible={this.handleToggleModalVisible}
                        imgPath={this.state.info.imgPath}
                    />
                </Content>
            )
        } else {
            info = <Content><Text></Text></Content>
        }
        return (
            <Container>
                {info}
            </Container>
        );
    }
}

const infoStyle = StyleSheet.create({
    ImgContainer: {
        borderColor: '#9B9B9B',
        borderWidth: 1 / PixelRatio.get(),
        alignItems: 'center',
        flexDirection: "row",
        flexWrap: "wrap"
    },
    Img: {
        width: 66,
        height: 66
    },
    new: {
        backgroundColor: "#ddd"
    },
    wx: {
        backgroundColor: "#dff",
        flexDirection: "column",
        alignItems: "flex-end",
    }
});