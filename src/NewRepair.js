import React, { Component } from 'react';
import { Button,View} from 'react-native';
import { createStackNavigator} from 'react-navigation';

import Main from './NewRepair/Main';
import Info from './NewRepair/RepairInfo';
import Add from './NewRepair/Add';
import AddNewRec from './NewRepair/AddNewRec';
// import Scan from './NewRepair/Scan';
import SearchInfo from './SearchByKey/SearchInfo';

const RootStack = createStackNavigator(
    {
        Main: {
            screen: Main
        },
        Info: {
            screen: Info,
            navigationOptions: {
                title: '报修详情',
            }
        },
        Add: {
            screen: Add,
            navigationOptions: {
                title: '新增报修',
            }
        },
        AddNewRec: {
            screen: AddNewRec,
            navigationOptions: {
                title: '补充记录',
            }
        },
        // Scan: {
        //     screen: Scan,
        //     navigationOptions: {
        //         title: '二维码/条码扫描',
        //     }
        // },
        SearchInfo: {
            screen: SearchInfo,
            navigationOptions: {
                title: '详情',
            }
        },
    }

);

export default class NewRepair extends Component {
    render() {
        return <RootStack />;
    }
}
