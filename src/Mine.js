import React, { Component } from 'react';
import { Button, Text, View, StyleSheet, TouchableWithoutFeedback } from 'react-native';

import { createStackNavigator } from 'react-navigation';
import { storage } from './components/storage';
import comm from './css/comm';
import Login from './Mine/Login';
import Url from './Mine/Url';

class MineScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: false,
            user: {},
            isUrl: false,
        };
    }
    componentWillMount() {
        this.refresh();
    }
    refresh = () => {
        storage.load('isLogin', (data) => {
            this.setState({ isLogin: data });
        })
        storage.load('user', (data) => {
            this.setState({ user: data });
        })
    }
    render() {
        var loginState = this.state.isLogin ?
            <Text style={style.username}>{this.state.user.username}</Text> :
            <Button style={style.loginButton}
                title="登录"
                onPress={() => {
                    this.props.navigation.navigate('Login', {
                        callback: () => {
                            this.refresh()
                        },
                    });
                }}
            />;
        var logout = this.state.isLogin ?
            <TouchableWithoutFeedback onPress={() => {
                this.setState({ isLogin: false });
                storage.save('isLogin', false);
            }}>
                <View style={style.logout}>
                    <Text style={style.logoutText}>登出</Text>
                </View>
            </TouchableWithoutFeedback> :
            <View />;
        return (
            <View style={comm.container}>
                <View style={style.loginState}>{loginState}</View>
                <View style={style.url}>
                    <TouchableWithoutFeedback onPress={() => {
                        this.props.navigation.navigate('Url', {
                            callback: () => {
                                this.refresh()
                            },
                        })
                    }}>
                        <Text style={style.urlText}>绑定Url</Text>
                    </TouchableWithoutFeedback>
                </View>
                {logout}
            </View>
        );
    }
}

const MineStack = createStackNavigator(
    {
        Mine: {
            screen: MineScreen,
            navigationOptions: {
                title: '个人中心',
            }
        },
        Login: {
            screen: Login,
            navigationOptions: {
                title: '登录',
            }
        },
        Url: {
            screen: Url,
            navigationOptions: {
                title: 'Url绑定',
            }
        }
    }

);

export default class Mine extends Component {
    render() {
        return <MineStack />;
    }
}

const style = StyleSheet.create({
    loginState: {
        backgroundColor: "white",
        height: 80,
        marginVertical: 10,
        justifyContent: 'center',
        alignItems: "center",
    },
    username: {
        fontSize: 30
    },
    loginButton: {
        width: 100,
        height: 50,
        fontSize: 20,
        justifyContent: 'center',
        alignItems: "center",
    },
    url: {
        backgroundColor: "white",
        height: 50,
        marginVertical: 10,
        paddingHorizontal: 10,
        justifyContent: 'center',
    },
    urlText: {
        fontSize: 20,
    },
    logout: {
        backgroundColor: "white",
        height: 50,
        marginTop: 50,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: "center",
        backgroundColor: "gray",
    },
    logoutText: {
        fontSize: 30,
        color: "white"
    },

});