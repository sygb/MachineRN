import { storage } from './index'

/**
 * sync方法的名字必须和所存数据的key完全相同
 * 方法接受的参数为一整个object，所有参数从object中解构取出
 * 这里可以使用promise。或是使用普通回调函数，但需要调用resolve或reject
 * @type {{user: ((params))}}
 */
const sync = {
  isUrl(params) {
    let { id, resolve, reject, syncParams: { extraFetchOptions, someFlag } } = params
    storage.save('isUrl', false)
    resolve && resolve(false)
  },
  url(params) {
    let { id, resolve, reject, syncParams: { extraFetchOptions, someFlag } } = params
    obj = {
      type: 'https',
      url: '127.0.0.1',
      port: '5001'
    }
    storage.save('url', obj)
    resolve && resolve(obj)
  },
  isLogin(params) {
    let { id, resolve, reject, syncParams: { extraFetchOptions, someFlag } } = params
    storage.save('isLogin',false)
    resolve && resolve(false)
  },
  user(params) {
    let { id, resolve, reject, syncParams: { extraFetchOptions, someFlag } } = params
    obj = {}
    storage.save('user', obj)
    resolve && resolve(obj)
  }
}

export { sync }