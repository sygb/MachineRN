import { storage } from '../storage';
import './config';

export default fun = {
    loadConfig (){
        storage.load('isUrl', (data) => {
            global.isUrl = data;
        })
        storage.load('url', (data) => {
            global.urltype = data.type;
            global.urlpath = data.path;
            global.urlport = data.port;
        })
        storage.load('isLogin', (data) => {
            global.isLogin = data;
        })
        storage.load('user', (data) => {
            global.userid = data.userid;
            global.username = data.username;
            global.token = data.token;
        })
    }
}