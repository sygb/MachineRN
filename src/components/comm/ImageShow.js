import React, { PureComponent } from 'react'
import { View, Modal } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';

export default class ImageShow extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            images:[],
            index: 0,
        };
    }
    componentWillMount(){
        imgs=[];
        imgArray = this.props.imgPath.split("^");
        type = this.props.imgtype;
        imgArray.map((itemObj) =>{
            path = itemObj;
            if(type != "local"){
                path = global.urltype + global.urlpath + ":" + global.urlport + path;
            }
            imgs.push({ url: path});
        })
        this.setState({ images: imgs})
    }
    render() {
        return (
            <View style={{padding: 10}}>
                <Modal
                    visible={this.props.modalVisible}
                    transparent={true}
                    onRequestClose={this.props.toggleModalVisible}>
                    <ImageViewer imageUrls={this.state.images}/>
                </Modal>
            </View>
        )
    }
}