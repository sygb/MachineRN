import React, { Component } from 'react';
import { Left, Right, ListItem, Text, View, Icon } from 'native-base';
import { StyleSheet, TouchableOpacity } from "react-native";
import moment from 'moment';

export default class RepairItem extends Component {
    render() {
        return (
            <ListItem>
                <Left>
                    <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('Info',
                            {
                                wxid: this.props.item.wxid,
                                callback: (type) => {
                                    this.props.changeFresh(type)
                                }
                            })
                    }}>
                        <View style={{ flexDirection: "row" }}>
                            <View style={style.kindStyle}>
                                <Text>{this.props.item.kind == "WX" ? "维修" : "保养"}</Text>
                            </View>
                            <View style={{ marginLeft: 10 }}>
                                <Text>
                                    {this.props.item.wxid}
                                </Text>
                                <Text>
                                    {this.props.item.macName}{this.props.item.modelNo}
                                </Text>
                                <Text style={{ fontSize: 16, color: "#666" }}>
                                    {this.props.item.mtncContent}
                                </Text>
                                <Text style={{fontSize:14,color:"#666"}}>
                                    {this.props.item.wxStatus == "WX" ? this.props.item.recName + "," : this.props.item.appName + ","}
                                    {moment(this.props.item.mtncDate).format("MM-DD HH:mm:ss")}
                                    {"  "+moment(this.props.item.mtncDate).fromNow()}
                                </Text>
                                
                            </View>
                        </View>
                    </TouchableOpacity>

                </Left>
            </ListItem>

        );
    }
}


const style = StyleSheet.create({
    kindStyle: {
        width: 50,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        borderColor: "lightblue",
        borderWidth: 1,
        borderRadius: 100,
        marginTop: 10,
        marginVertical: 10,
    }
})