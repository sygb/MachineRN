import React, { Component } from 'react';
import { TouchableNativeFeedback, StyleSheet, Text, View } from 'react-native';

import { Container, Content, List, Left, Right, Radio, ListItem} from 'native-base';
import { DotIndicator } from 'react-native-indicators';
import RepairItem from './RepairItem';

export default class RepairList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isLast: false,
            list: [],
            sr: 1,
            rn: 10,
            aboutme:false,
        };
    }
    componentDidMount() {
        setTimeout(() => {
            this.handelLoadList();
        }, 200);
    }
    handelLoadList = () => {
        if (!global.isUrl) {
            alert('请先设置url');
            return
        }
        if (!global.isLogin) {
            alert('请先登录');
            return
        }
        this.setState({ loading: true });

        url = global.urltype + global.urlpath + ":" + global.urlport + "/api/MachineWX/getListByModify";

        para = 'status=' + this.props.status
            + '&pageIndex=' + this.state.sr
            + '&pageSize=' + this.state.rn
            + '&aboutMe=' + this.state.aboutme
            ;
        url += '?' + para;
        // alert(url);
        setTimeout(() => {
            return fetch(url, {
                method: 'GET',
                headers: {
                    'Authorization': global.token
                }
            }).then(response => response.json())
                .then(json => {
                    var newstate = {};
                    if (json.count > (this.state.sr - 1) * this.state.rn) {
                        if (this.state.sr==0){
                            newstate.list = json.data;
                        }else{
                            newstate.list = this.state.list.concat(json.data);
                        }
                        
                    }
                    if (json.count > this.state.sr * this.state.rn) {
                        newstate.sr = this.state.sr + 1;
                    } else {
                        newstate.isLast = true;
                    }
                    newstate.loading = false;
                    this.setState(newstate);
                })
                .catch(error => {
                    this.setState({ loading: false });
                    console.error(error);
                });
        }, 600)

    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.isFresh) {
            this.setState({
                loading: false,
                isLast: false,
                list: [],
                sr: 1,
                rn: 10,
            })
            this.props.changeFalse(nextProps.status)
            setTimeout(() => {
                this.handelLoadList()
            }, 200);
        }
    }
    handleChangeAboutMe=()=>{
        this.setState({ 
            loading: false,
            isLast: false,
            list: [],
            sr: 1,
            rn: 10,
            aboutme: !this.state.aboutme
        })
        setTimeout(() => {
            this.handelLoadList()
        }, 200);
    }
    render() {
        if (this.props.status == "WX"){
            aboutme = <ListItem>
                <Left>
                    <Text>跟我有关</Text>
                </Left>
                <Right>
                    <Radio selected={this.state.aboutme} onPress={this.handleChangeAboutMe}/>
                </Right>
            </ListItem>
        }else{
            aboutme = <View></View>
        }
        if (this.state.loading) {
            StatusBar = <DotIndicator color='lightgray' />
        } else {
            if (this.state.isLast) {
                StatusBar = <Text>没有更多资料了</Text>
            } else {
                StatusBar = <TouchableNativeFeedback onPress={this.handelLoadList}>
                    <Text style={{ margin: 30 }}>加载更多</Text>
                </TouchableNativeFeedback>
            }
        }
        return (
            <Container>
                <Content>
                    {aboutme}
                    <List>
                        {
                            this.state.list.map((itemObj, idx) => {
                                return (
                                    <RepairItem key={idx} item={itemObj} 
                                        navigation={this.props.navigation} 
                                        changeFresh={this.props.changeTrue}/>
                                )
                            })
                        }
                    </List>
                    <View style={style.statusBarView}>
                        {StatusBar}
                    </View>
                </Content>
            </Container>
        );
    }
}

const style = StyleSheet.create({
    statusBarView: {
        justifyContent: 'center',
        alignItems: "center",
        height: 50,
    }
})