import React, { Component } from 'react';
import { Container, Tab, Tabs, View} from 'native-base';

import RepairList from './RepairList';
import SearchByKey from '../SearchByKey';

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isNEWFresh: false,
            isWXFresh: false,
        };
    }
    handelFreshTrue = (type) => {
        if(type == 'add'){
            this.setState({
                isNEWFresh: true,
                isWXFresh: true,
            });
        }
        if (type == 'finish') {
            this.setState({
                isWXFresh: true,
            });
        }
    }
    handelFreshFalse = (status) => {
        if(status == "NEW"){
            this.setState({
                isNEWFresh: false,
            })
        }
        if (status == "WX") {
            this.setState({
                isWXFresh: false,
            })
        }
    }
    handleChangeTab=(index)=>{
        if(index==0){
            this.setState({
                isNEWFresh: true,
            })
        }
        if (index == 1) {
            this.setState({
                isWXFresh: true,
            })
        }
    }
    render() {
        return (
            <Container>
                <Tabs onChangeTab={(i) => {this.handleChangeTab(i.i)}}>
                    <Tab tabStyle={{ backgroundColor: "#ff0000" }} activeTabStyle={{ backgroundColor: "#ff0000" }} heading="新工单">
                        <RepairList status="NEW" 
                            navigation={this.props.navigation}
                            isFresh={this.state.isNEWFresh}
                            changeTrue={this.handelFreshTrue}
                            changeFalse={this.handelFreshFalse}/>
                    </Tab>
                    <Tab tabStyle={{ backgroundColor: "#ff0000" }} activeTabStyle={{ backgroundColor: "#ff0000" }} heading="维修中">
                        <RepairList status="WX" 
                            navigation={this.props.navigation}
                            isFresh={this.state.isWXFresh}
                            changeTrue={this.handelFreshTrue}
                            changeFalse={this.handelFreshFalse} />
                    </Tab>
                    <Tab tabStyle={{ backgroundColor: "#ff0000" }} activeTabStyle={{ backgroundColor: "#ff0000" }} heading="查询">
                        <SearchByKey navigation={this.props.navigation} bgcolor="#ff0000"/>
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}