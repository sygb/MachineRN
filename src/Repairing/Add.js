import React, { Component } from 'react';
import { StyleSheet, View, Text, Alert} from 'react-native';
import { Container, Content, Form, Textarea, Button, Item, Label, Input} from 'native-base';
export default class FloatingLabelExample extends Component {
    constructor(props) {
        super(props);
        this.state = {
            WXContent: "",
        };
    }
    goback = () => {
        const { navigate, goBack, state } = this.props.navigation;
        state.params.callback();
        this.props.navigation.goBack();
    }
    handleSubmit=()=>{
        if (this.state.WXContent == ""){
            Alert.alert("错误提示", "请填写补充说明", [{ text: '关闭' }])
            return;
        }
        const { navigation } = this.props;
        url = global.urltype + global.urlpath + ":" + global.urlport + "/api/MachineWXDetail/Add";
        $para = 'wxid=' + navigation.getParam('wxid') 
            + '&WXContent=' + this.state.WXContent
            + '&WXMaterial=' + this.state.WXMaterial
            + '&Cost=' + this.state.Cost
            + '&Kind=WX';

        return fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': global.token
            },
            body: $para
        })
            .then(response => response.json())
            .then(json => {
                if (json){
                    Alert.alert("提交成功", "提交成功", [
                        {
                            text: '确认',
                            onPress: () => {
                                this.goback();
                            }
                        }
                    ]);
                }else{
                    Alert.alert("提交失败", "提交失败，请稍后再试", [
                        {
                            text: '确认'
                        }
                    ]);
                }
            })
            .catch(error => {
                console.error(error);
            });

    }
    render() {
        return (
            <Container>
                <Content>
                    <Form>
                        <Textarea rowSpan={10} bordered placeholder="维修描述" 
                            value={this.state.WXContent}
                            onChangeText={(WXContent) => this.setState({ WXContent})} />
                    </Form>
                    <View style={addStyle.ButtonView}>
                        <Button default block onPress={this.handleSubmit}>
                            <Text style={addStyle.ButtonText}> 提交 </Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

const addStyle = StyleSheet.create({
    ButtonView: {
        paddingHorizontal: 10,
        paddingVertical: 20,
    },
    ButtonText: {
        color: "#fff",
    }
});