import React, { Component } from 'react';
import { View, Text, StyleSheet, Alert } from 'react-native';
import { Container, Button, Content, Form, Item, Input, Label } from 'native-base';
import { storage } from '../components/storage';

export default class Url extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isUrl: false,
            urltype: "http://",
            urlpath: "127.0.0.1",
            urlport: "80",
        };
    }
    componentDidMount() {
        storage.load('isUrl', (data) => {
            this.setState({ isUrl: data });
        })
        storage.load('url', (data) => {
            this.setState({
                urltype: data.type,
                urlpath: data.path,
                urlport: data.port,
            });
        })
    }
    goback = () => {
        const { navigate, goBack, state } = this.props.navigation;
        state.params.callback();
        this.props.navigation.goBack();
    }
    checkUrl = () => {
        if (this.state.urltype == "" || this.state.urlpath == "" || this.state.urlport == "") {
            Alert.alert("请填写完整信息")
            return;
        }
        //验证
        url = this.state.urltype + this.state.urlpath + ":" + this.state.urlport + "/api/System/Test";
        return fetch(url)
            .then(response => response.json())
            .then(json => {
                if (json.recode == 1) {
                    this.setState({ isUrl: true });
                    global.isUrl = true;
                    global.urltype = this.state.urltype;
                    global.urlpath = this.state.urlpath;
                    global.urlport = this.state.urlport;
                    storage.save('isUrl', true);
                    storage.save('url', {
                        type: this.state.urltype,
                        path: this.state.urlpath,
                        port: this.state.urlport
                    });
                    Alert.alert("验证成功", "接口验证成功", [
                        {
                            text: '确认',
                            onPress: () => {
                                this.goback();
                            }
                        }
                    ]);
                } else {
                    Alert.alert("接口验证失败")
                }
            })
            .catch(error => {
                console.error(error);
            });
    }
    render() {
        return (
            <Container>
                <Content>
                    <Form>
                        <Item floatingLabel>
                            <Label>http类型</Label>
                            <Input
                                value={this.state.urltype}
                                onChangeText={(urltype) => this.setState({ urltype })}
                            />
                        </Item>
                        <Item floatingLabel last>
                            <Label>网址</Label>
                            <Input
                                value={this.state.urlpath}
                                onChangeText={(urlpath) => this.setState({ urlpath })} />
                        </Item>
                        <Item floatingLabel last>
                            <Label>端口</Label>
                            <Input
                                value={this.state.urlport}
                                onChangeText={(urlport) => this.setState({ urlport })} />
                        </Item>
                    </Form>
                    <View style={style.ButtonView}>
                        <Button default block onPress={this.checkUrl}>
                            <Text style={style.ButtonText}> 验证 </Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

const style = StyleSheet.create({
    ButtonView: {
        paddingHorizontal: 10,
        paddingVertical: 20,
    },
    ButtonText: {
        color: "#fff",
    }
});