import React, { Component } from 'react';
import { View, Text, StyleSheet, Alert } from 'react-native';
import { Container, Button, Content, Form, Item, Input, Label } from 'native-base';
import { storage} from '../components/storage';

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            psd: "",
        };
    }
    goback = () => {
        const { navigate, goBack, state } = this.props.navigation;
        state.params.callback();
        this.props.navigation.goBack();
    }
    login = () => {
        if (!global.isUrl){
            Alert.alert('请先配置url信息');
            return;
        }

        if (this.state.username == "" || this.state.psd == "") {
            Alert.alert("错误提示", "账号和密码不能为空", [{ text: '关闭' }])
            return;
        }
        //登录
        url = global.urltype + global.urlpath + ":" + global.urlport +"/api/User/Login";
        $para = 'username=' + this.state.username + '&psd=' + this.state.psd;
        return fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: $para
        })
            .then(response => response.json())
            .then(json => {
                if (json.recode == 1) {
                    //保存登录数据
                    global.username = json.info.username;
                    global.userid = json.info.userid;
                    global.token = json.info.token;
                    global.userid = json.info.userid;
                    global.isLogin = true;
                    user = {
                        userid: json.info.userid,
                        username: json.info.username,
                        token: json.info.token
                    }
                    storage.save('user', user);
                    storage.save('isLogin', true);
                    Alert.alert("登录状态", "登录成功",[
                        {
                            text: '确认',
                            onPress: () => {
                                this.goback();
                            }
                        }
                    ]);
                } else if (json.recode == 0) {
                    Alert.alert(json.msg);
                } else {
                    Alert.alert("系统异常");
                }
            })
            .catch(error => {
                console.error(error);
            });


    }
    render() {
        return (
            <Container>
                <Content>
                    <Form>
                        <Item floatingLabel>
                            <Label>账号</Label>
                            <Input
                                value={this.state.username}
                                onChangeText={(username) => this.setState({ username })}
                            />
                        </Item>
                        <Item floatingLabel last>
                            <Label>密码</Label>
                            <Input
                                value={this.state.psd}
                                onChangeText={(psd) => this.setState({ psd })} />
                        </Item>
                    </Form>
                    <View style={style.ButtonView}>
                        <Button default block onPress={this.login}>
                            <Text style={style.ButtonText}> 登录 </Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

const style = StyleSheet.create({
    ButtonView: {
        paddingHorizontal: 10,
        paddingVertical: 20,
    },
    ButtonText: {
        color: "#fff",
    }
});