import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

import BarcodeScanner from 'react-native-barcode-scanner-universal'

export default class Scan extends Component {
    //解析数据
    parseData(pdata) {
        var ptype = pdata.type;
        var data = pdata.data;

        fetch(data)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        let scanArea = null
        scanArea = (
            <View style={styles.rectangleContainer}>
                <View style={styles.rectangle} />
            </View>
        )

        return (
            <BarcodeScanner
                onBarCodeRead={this.parseData.bind(this)}
                style={styles.camera}
            >
                {scanArea}
            </BarcodeScanner>
        );
    }
}

const styles = StyleSheet.create({
    camera: {
        flex: 1
    },
    rectangleContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent'
    },
    rectangle: {
        height: 250,
        width: 250,
        borderWidth: 2,
        borderColor: '#00FF00',
        backgroundColor: 'transparent'
    }
});