import React, { Component } from 'react';
import { Container, Tab, Tabs, View} from 'native-base';
import EvilIcons from 'react-native-vector-icons/Ionicons';

import RepairList from './RepairList';
import SearchByKey from '../SearchByKey';

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isNEWFresh: false,
            isWXFresh: false,
        };
    }
    static navigationOptions = ({ navigation }) => {

        return {
            title:'报修',
            headerRight: (
                <View style={{ paddingRight: 20 }}>
                    <EvilIcons
                        name={'ios-add'}
                        size={40}
                        style={{ color: "blue" }}
                        onPress={navigation.getParam('add', {
                            callback: () => {
                                this.handelChangeNEWFresh()
                            },
                        })}
                    />
                </View>
            ),
        };
    };
    handelChangeNEWFresh=()=>{
        this.setState({ isNEWFresh: !this.state.isNEWFresh })
    }
    handelChangeWXFresh = () => {
        this.setState({ isWXFresh: !this.state.isWXFresh })
    }
    componentDidMount() {
        this.props.navigation.setParams({ add: this.handeladd });
    }
    handeladd = () => {
        this.props.navigation.navigate('Add', {
            callback: () => {
                this.handelChangeNEWFresh()
            },
        })
    }
    handleChangeTab = (index) => {
        if (index == 0) {
            this.setState({
                isNEWFresh: true,
            })
        }
        if (index == 1) {
            this.setState({
                isWXFresh: true,
            })
        }
    }
    render() {
        return (
            <Container>
                <Tabs onChangeTab={(i) => { this.handleChangeTab(i.i) }}>
                    <Tab heading="新工单">
                        <RepairList status="NEW" 
                            navigation={this.props.navigation} 
                            isFresh={this.state.isNEWFresh}
                            changeFresh = {this.handelChangeNEWFresh}/>
                    </Tab>
                    <Tab heading="维修中">
                        <RepairList status="WX" 
                            navigation={this.props.navigation}
                            isFresh={this.state.isWXFresh}
                            changeFresh={this.handelChangeWXFresh}/>
                    </Tab>
                    <Tab heading="查询">
                        <SearchByKey navigation={this.props.navigation} bgcolor="#4054b2"/>
                    </Tab>
                </Tabs>
            </Container>
        );
    }
}