import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';

import Main from './Repairing/Main';
import Info from './Repairing/RepairInfo';
import Add from './Repairing/Add';
import SearchInfo from './SearchByKey/SearchInfo';

const RepairingStack = createStackNavigator(
    {
        Main: {
            screen: Main,
            navigationOptions: {
                title: '维修',
            }
        },
        Info: {
            screen: Info,
            navigationOptions: {
                title: '报修详情',
            }
        },
        Add: {
            screen: Add,
            navigationOptions: {
                title: '维修记录',
            }
        },
        SearchInfo: {
            screen: SearchInfo,
            navigationOptions: {
                title: '详情',
            }
        },
    }

);

export default class Repairing extends Component {
    render() {
        return <RepairingStack />;
    }
}
