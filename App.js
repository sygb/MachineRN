import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import NewRepair from './src/NewRepair';
import Repairing from './src/Repairing';
import Mine from './src/Mine';
import fun from './src/components/comm/function';

console.disableYellowBox = true;
fun.loadConfig();

const RootStack = createBottomTabNavigator(
{
    NewRepair: {
      screen: NewRepair,
      navigationOptions: {
        tabBarPosition: 'bottom',
        tabBarLabel: '报修',
        showLabel: false,
        tabBarIcon: ({ tintColor, focused }) => (
          <EvilIcons
            name={'bell'}
            size={30}
            style={{ color: tintColor }}
          />
        ),
      }
    },
    Repairing: {
      screen: Repairing,
      navigationOptions: {
        tabBarPosition: 'bottom',
        tabBarLabel: '维修',
        showLabel: false,
        tabBarIcon: ({ tintColor, focused }) => (
          <EvilIcons
            name={'gear'}
            size={30}
            style={{ color: tintColor }}
          />
        ),
      }
    },
    Mine: {
      screen: Mine,
      navigationOptions: {
        tabBarPosition: 'bottom',
        tabBarLabel: '我的',
        showLabel: false,
        tabBarIcon: ({ tintColor, focused }) => (
          <EvilIcons
            name={'user'}
            size={30}
            style={{ color: tintColor }}
          />
        ),
      }
    },
  },
  {
    initialRouteName: 'NewRepair',
  }
);

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <RootStack />;
  }
}